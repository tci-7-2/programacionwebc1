package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ConversionActivity extends AppCompatActivity {

    private TextView lblres;
    private EditText txtGrados;
    private RadioGroup groupConv;
    private RadioButton ckCeltoFa, ckFatoCel;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conversion);
        iniciarComponentes();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        lblres = findViewById(R.id.lblres);
        txtGrados = findViewById(R.id.txtGrados);
        groupConv = findViewById(R.id.groupConv);
        ckCeltoFa = findViewById(R.id.ckCeltoFa);
        ckFatoCel = findViewById(R.id.ckFatoCel);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Evento para limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtGrados.setText("0000");
                lblres.setText("0000");
            }
        });

        // Evento para regresar al Menú
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });

        // Evento para realizar la conversión
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Conversion();
            }
        });
    }

    public void Conversion(){
        String gradosSt = txtGrados.getText().toString();
        if (gradosSt.matches("") || groupConv.getCheckedRadioButtonId() == -1){
            Toast.makeText(getApplicationContext(),"¡Por favor rellene los datos faltantes o seleccione alguna opción!",Toast.LENGTH_SHORT).show();
        } else {
            try {
                double grados = Double.parseDouble(gradosSt);
                if (ckCeltoFa.isChecked()){
                    double res = (grados * 9/5) + 32;
                    lblres.setText(String.format("%.2f °F", res));
                } else if (ckFatoCel.isChecked()) {
                    double res = (grados - 32) * 5/9;
                    lblres.setText(String.format("%.2f °C", res));
                }
            } catch (NumberFormatException e){
                Toast.makeText(getApplicationContext(), "Ingrese valores numéricos válidos", Toast.LENGTH_SHORT).show();
            }
        }
    }
}